# sisop-praktikum-modul-1-2023-AM-B10

Anggota :
| Nama                         | NRP        |
|------------------------------|------------|
|Raden Pandu Anggono Rasyid    | 5025201024 |
|Ryan Abinugraha               | 5025211178 |
|Abhinaya Radiansyah Listiyanto| 5025211173 |

# Penjelasan Soal Nomor 1

# Penjelasan Soal Nomor 2

# Penjelasan Soal Nomor 3

Code for louis.sh :
``` 
#!/bin/bash

read -p "Enter username: " username
read -p "Enter password: " -s password

# check if password meets the criteria
if [[ ${#password} -lt 8 || !("$password" =~ [A-Z]) || !("$password" =~ [a-z]) || !("$password" =~ [0-9]) || "$password" =~ $username || "$password" =~ chicken || "$password" =~ ernie ]]; then
    echo -e "\nPassword does not meet the criteria"
    exit 1
fi

# check if the username already exists in the users file
if grep -q "^$username:" ./users/users.txt; then
    echo "REGISTER: ERROR User already exists" >> ./users/log.txt
    echo "User already exists"
    exit 1
fi

# add the username and password to the users file
echo "$username:$password" >> ./users/users.txt
echo "REGISTER: INFO User $username registered successfully" >> ./users/log.txt
echo "User registered successfully"
```
Code for retep.sh :
```
#!/bin/bash

read -p "Enter username: " username
read -p "Enter password: " -s password

# check if the username and password match
if ! grep -q "^$username:$password$" ./users/users.txt; then
    echo "LOGIN: ERROR Failed login attempt on user $username" >> ./users/log.txt
    echo "Invalid username or password"
    exit 1
fi

echo "LOGIN: INFO User $username logged in" >> ./users/log.txt
echo "Welcome, $username"

```
# Penjelasan Soal Nomor 4
