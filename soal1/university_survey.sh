#Menampilka 5 Universitas dengan ranking tertinggi di Jepang
grep "Japan" 2023\ QS\ World\ University\ Rankings.csv | head -n 5

#Menampilkan Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang.
grep "Japan" 2023\ QS\ World\ University\ Rankings.csv | head -n 5 | sort -r -t ',' -k 9

#Menampilkan 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
grep "Japan" 2023\ QS\ World\ University\ Rankings.csv | head -n 10 | sort -k 20

#Menampilkan universitas dengan kata kunci keren.
grep -i "keren" 2023\ QS\ World\ University\ Rankings.csv 
