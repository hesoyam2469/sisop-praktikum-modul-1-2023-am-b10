#!/bin/bash
hour=$(date +"%-H")

#membuat interval menjadi jam
INTERVAL = $1

#menghitung jam sekarang
if [$hour -eq 0]; then
 count=1
else
 count=$hour
fi

#membuat folder kumpulan
j=1
mkdir kumpulan_$j

#looping mendownload gambar
while true;do
 for ((i=1; i<=$count; i++)); do
  echo >> kumpulan_$j/perjalanan_$i
done

#tidak jalan selama 10 jam
  sleep 36000

#looping kumpulan selanjutnya
  hour=$(date +"%-H")
  if ((hour % INTERVAL == 0))
  then
    j=$((j + 1))
    mkdir kumpulan_$j
  fi
done

#membuat zip dalam satu hari
TODAY=$(date +%d)
if [ ! -f "devil_${j}.zip" ] || [ "$TODAY" != "$(date -r devil_${j}.zip +%d)" ]
then
  zip -r "devil_${j}.zip" "kumpulan_$j"
fi
